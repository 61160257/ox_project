import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UnitTest {
	
	@Test
	void showBoard() {
		Player O = null;
	    Player X = null;
		Game g = new Game();
		Board b = new Board(O,X);
		b.getBoard();
	}
	
	@Test
	void showTurn() {
		Player O = null;
	    Player X = null;
		Game g = new Game();
		Board b = new Board(O,X);
		b.getCurrentPlayer();
	}
	
	@Test
	void showWin() {
		Player O = null;
	    Player X = null;
		Game g = new Game();
		Board b = new Board(O,X);
		b.getWinner();
	}
	@Test
	void showDraw() {
		Player O = null;
	    Player X = null;
		Game g = new Game();
		Board b = new Board(O,X);
		g.showDraw();
	}
	
	@Test
	void showResult() {
		Player O = null;
	    Player X = null;
		Game g = new Game();
		Board b = new Board(O,X);
		
		if(b.getWinner()!=null){
            this.showBoard();
            showWin();
        }else{
            this.showBoard();
            showDraw();
        }
	}
	
	@Test
	void showState() {
		Player O = null;
	    Player X = null;
		Game g = new Game();
		Board b = new Board(O,X);
		g.showStat();
	}
	
	@Test
	void switchPlayer() {
		Player O = null;
	    Player X = null;
		Game g = new Game();
		Board b = new Board(O,X);
		b.switchPlayer();
	}
	
	@Test
	void showlose(){
		Player O = new Player('O');
		Player X = new Player('X');
		Game g = new Game();
		Board b = new Board(O,X);
		O.getLose();
		
	}
	
	@Test
	void checkCrosswiseR(){
		Player O = new Player('O');
		Player X = new Player('X');
		Game g = new Game();
		Board b = new Board(O,X);
		b.setRowCol(1, 3);
		b.setRowCol(1, 2);
		b.setRowCol(1, 1);
		b.checkCrosswiseR();
		
	}
	
	@Test
	void checkCrosswiseL(){
		Player O = new Player('O');
		Player X = new Player('X');
		Game g = new Game();
		Board b = new Board(O,X);
		b.setRowCol(1, 1);
		b.setRowCol(2, 2);
		b.setRowCol(3, 3);
		b.checkCrosswiseL();
		
	}
	
	
	
	
	

}
