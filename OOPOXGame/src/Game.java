import java.util.*;
import java.util.Scanner;
public class Game {
	private Scanner kb = new Scanner(System.in);
	Board board = null;
    Player O = null;
    Player X = null;
    private int row,col;
    
	public Game(){
		this.O=new Player('O');
        this.X=new Player('X');
	}

	public void run() {
		while(true){
            this.runGame();
            if(askContinue()){
                return;
            }
        }
		
	}
	private int getRandomNumber(int min,int max){
        return (int)((Math.random()*(max-min))+min);
    }

	public boolean askContinue() {
		while(true){
            System.out.print("Continue y/n?:");
           String ans = kb.next();
           if(ans.equals("n")){
               return true;
           }
           if(ans.equals("y")){
               return false;
           }
       }
	}

	private void runGame() {
		this.newGame();
		while(true){
			this.showBoard();
			this.showTurn();
			inputRowCol();
            if(board.checkWin()){
                showResult();
                showStat();
                return;
		}
            board.switchPlayer();
	}
	}
	public void showStat() {
		System.out.println(O.getName()+"(win,lose,draw):"+O.getWin()+","+O.getLose()+","+O.getDraw());
        System.out.println(X.getName()+"(win,lose,draw):"+X.getWin()+","+X.getLose()+","+X.getDraw());
    }   

	public void showResult() {
		if(board.getWinner()!=null){
            this.showBoard();
            showWin();
        }else{
            this.showBoard();
            showDraw();
        }
	}

	public void showDraw() {
		System.out.println("Draw!!!");
		
	}

	private void showWin() {
		System.out.println(board.getWinner().getName()+" Win!!!");
		
	}

	private void inputRowCol() {
		while(true){
            this.inputOX();
            try{
               if(board.setRowCol(row,col)){
                return;
            } 
            }catch(ArrayIndexOutOfBoundsException e){
                System.out.println("Please input number 1-3");
            }
        }
		
	}

	private void inputOX() {
		while(true){
            try{
                System.out.print("Please input row col:");
                row = kb.nextInt();
                col = kb.nextInt();
                return;
            }catch(InputMismatchException e){
                System.out.println("Please inpur number 1-3");
                kb.next();
            }
        }
		
	}

	public void showTurn() {
		System.out.println("Turn "+((board.getCurrentPlayer()).getName()));
	}

	public void showBoard() {
		char[][] board = this.board.getBoard();
        for(int row=0;row<board.length;row++){
            System.out.print("|");
            for(int col=0;col<board.length;col++){
                System.out.print(" "+board[row][col]+" |");
            }
            System.out.println("");
        }
	}

	private void newGame() {
		if(getRandomNumber(1, 100)%2==0){
            this.board=new Board(O, X);
        }else{
            this.board=new Board(X, O);
        }
	}

}
