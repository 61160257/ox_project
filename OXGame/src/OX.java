
import java.util.*;

public class OX {

	static char[][] tableOX = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	static char player = 'O';
	static int countturn = 0;

	public static void showWelcome() {
		System.out.println("Welcome to OX Game");
	}

	public static void showTable() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(tableOX[i][j] + "  ");
			}
			System.out.println();
		}

	}

	public static void showTurn() {
		System.out.println("turn " + player);
		inputRowCol();
	}

	public static void inputRowCol() {
		Scanner ip = new Scanner(System.in);
		System.out.print("Please input row and column : ");
		try {
			int inputRow = ip.nextInt();
			int inputCol = ip.nextInt();
			if (checkError(inputRow, inputCol) == true) {
				inputRowCol();
			} else {
				inputTableOX(inputRow, inputCol);
			}
		} catch (InputMismatchException e) {
			System.out.println("Input again");
		}
	}

	public static boolean checkError(int inputRow, int inputCol) {
		if (inputRow > 3 || inputRow < 1 || inputCol > 3 || inputCol < 1) {
			System.out.println("Error!! Row : Please input 1 , 2 , 3");
			return true;
		}
		return false;
	}

	public static void inputTableOX(int inputRow, int inputCol) {
		tableOX[inputRow - 1][inputCol - 1] = player;
		countturn++;
		checkWin();
		if (checkWin() == false)
			switchPlayer();

	}

	public static void switchPlayer() {
		if (player == 'O') {
			player = 'X';
		} else if (player == 'X') {
			player = 'O';
		}
	}

	public static boolean checkWin() {
		if (checkWinRow()) {
			return true;
		} else if (checkWinCol()) {
			return true;
		} else if (checkWinCrosswise()) {
			return true;
		}
		return false;
	}

	

	public static boolean checkDraw() {
		if (countturn == 9 && checkWin() == false) {
			return true;
		}
		return false;
	}
public static boolean Draw() {
		if (checkDraw()) {
			return true;
		}
		return false;
	}
	public static boolean checkWinRow() {
		if (tableOX[0][0] == player && tableOX[0][1] == player && tableOX[0][2] == player) {
			return true;
		} else if (tableOX[1][0] == player && tableOX[1][1] == player && tableOX[1][2] == player) {
			return true;
		} else if (tableOX[2][0] == player && tableOX[2][1] == player && tableOX[2][2] == player) {
			return true;
		}

		return false;
	}

	public static boolean checkWinCol() {
		if (tableOX[0][0] == player && tableOX[1][0] == player && tableOX[2][0] == player) {
			return true;
		} else if (tableOX[0][1] == player && tableOX[1][1] == player && tableOX[2][1] == player) {
			return true;
		} else if (tableOX[0][2] == player && tableOX[1][2] == player && tableOX[2][2] == player) {
			return true;
		}

		return false;
	}

	public static boolean checkWinCrosswise() {
		if (checkWinCrosswise1()) {
			return true;
		} else if (checkWinCrosswise2()) {
			return true;
		}
		return false;
	}

	public static boolean checkWinCrosswise1() {
		for (int i = 0; i < tableOX.length; i++) {
			if (tableOX[i][i] != player) {
				return false;
			}
		}
		return true;
	}

	public static boolean checkWinCrosswise2() {
		for (int i = 0; i < tableOX.length; i++) {
			if (tableOX[i][2 - i] != player) {
				return false;
			}
		}
		return true;
	}

	public static void showWinner() {
		if (checkWin()) {
			showTable();
			System.out.println("Player " + player + " is winner!!\n");
		} else if (Draw()) {
			showTable();
			System.out.println("Player O ties Player X!!\n");
		}
	}

	public static void main(String[] args) {
		showWelcome();
		while (true) {
			showTable();
			showTurn();
			if (checkWin() || Draw()) {
				break;
			}

		}
		showWinner();
	}

}
