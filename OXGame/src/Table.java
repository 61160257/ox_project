public class Table {
    
    static char whoWin;
    char[][] board = new char[3][3];
    
Table(){
        this.setGame();
    }
    
    void setGame(){
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = '-';
            } 
        }
    }

    boolean checkWin( int count, boolean finish) {
        this.printBoard();
        int countp1 = 0;
        int countp2 = 0;
        if (count >= 4) {
            for (int i = 0; i < 3; i++) {
                int count_row = 0;
                int count_col = 0;
                if ((board[0][0] == board[i][i]) && (board[0][0] != '-')) {
                    countp1 += 1;
                    if (countp1 == 3) {
                        finish = true;
                        System.out.println(" " + board[0][0] + " Win");
                        whoWin =  board[0][0];
                    }
                }
                if ((board[0][2] == board[i][2 - i]) && (board[0][2] != '-')) {
                    countp2 += 1;
                    if (countp2 == 3) {
                        finish = true;
                        System.out.println(" " + board[0][2] + " Win");
                        whoWin = board[0][2];
                    }
                }
                for (int j = 2; j > 0; j--) {
                    if ((board[i][0] == board[i][j]) && (board[i][0] != '-')) {
                        count_col += 1;
                        if (count_col == 2) {
                            finish = true;
                            System.out.println(" " + board[i][0] + " Win");
                            whoWin = board[i][0];
                        }
                    }
                    if ((board[0][i] == board[j][i]) && (board[0][i] != '-')) {
                        count_row += 1;
                        if (count_row == 2) {
                            finish = true;
                            System.out.println(" " + board[0][i] + " Win");
                            whoWin = board[0][i];
                        }
                    }
                }
            }
        }
        finish = checkDraw(count, finish);
        return finish;
    }
    
        static boolean checkDraw(int count, boolean finish) {
        if (count == 9) {
            finish = true;
            System.out.println("Draw!!!");
            whoWin = 'd';
        }
        return finish;
    }
    
    
    void inputX( int r, int c) {
        if (this.board[r - 1][c - 1] == '-') {
            this.board[r - 1][c - 1] = 'X';
        } else {
            System.out.println("used");
        }

    }
void inputO(int r, int c) {
        if (this.board[r - 1][c - 1] == '-') {
            this.board[r - 1][c - 1] = 'O';
        } else {
            System.out.println("used");
        }

    }
    void printBoard(){
        System.out.println("  1 2 3");
        for (int i = 0; i < board.length; i++) {
            System.out.print( (i+1) + " ");
            for (int j = 0; j < board[i].length; j++) {
                System.out.print( board[i][j] + " ");
            } 
            System.out.println("");
        }
    }
    
    char whoWin(){
        return whoWin;
    }

}
