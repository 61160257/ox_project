class Player {

	Table board;
	char whoWin;
	private int xW;
	private int xL;
	private int xD;
	private int oW;
	private int oL;
	private int oD;

	Player() {
		xW = 0;
		oW = 0;
		xL = 0;
		oL = 0;
		xD = 0;
		oD = 0;
	}

	void setScore() {
		char win = board.whoWin();

		if (win == 'O') {
			this.setoWin();
			this.setxLost();
		} else if (win == 'X') {
			this.setxWin();
			this.setoLost();
		} else if (win == 'd') {
			this.setDraw();
		} else {
			System.out.println("Something Error!");
		}
	}

	void setxLost() {
		this.xL = (xL + 1);
	}

	void setxWin() {
		this.xW = (xW + 1);
	}

	void setoLost() {
		this.oL = (oL + 1);
	}

	void setoWin() {
		this.oW = (oW + 1);
	}

	void setDraw() {
		this.xD = (xD + 1);
		this.oD = (oD + 1);
	}

	void printScore() {
		System.out.println("Player 0  Win : " + oW + " Lost : " + oL + " Draw : " + oD);
		System.out.println("Player X  Win : " + xW + " Lost : " + xL + " Draw : " + xD);

	}

}
