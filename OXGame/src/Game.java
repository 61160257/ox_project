import java.util.Scanner;

class Game {

    Scanner kb = new Scanner(System.in);
    private Table board;
    private Player player;
    Player o;
    Player x;

    void printWelcome() {
        System.out.println("Welcome to OX Game");
  
    }
    void turnX() {
        System.out.println("Turn X");
        System.out.print("Please input row, col : ");
        board.inputX(kb.nextInt(), kb.nextInt());
    }
 void turnO() {
        System.out.println("Turn O");
        System.out.print("Please input row, col : ");
        board.inputO(kb.nextInt(), kb.nextInt());
    }
    void turnGame() {
        player = new Player();
        board = new Table();
        Scanner kb = new Scanner(System.in);
        boolean finish = false;
        int count = 0;
        do {
            count++;
            this.turnO();
            finish = board.checkWin(count, finish);

            if (finish == true || count == 9) {
                break;
            }

            count++;
            this.turnX();
            finish = board.checkWin(count, finish);

        } while (!finish);
    }


    char goContinue() {
        char input;
        do {
            System.out.print("Continue ? (y/n) : ");
            input = kb.next().charAt(0);

            if (input == 'n' || input == 'y') {
                break;
            }

        } while (true);

        return input;
    }
    void run() {
        board = new Table();
        this.printWelcome();
        char play;
        do {
            board.setGame();
            board.printBoard();
            this.turnGame();
            player.setScore();
            player.printScore();
            play = this.goContinue();
        } while (play == 'y');
    }

}
