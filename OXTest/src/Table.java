public class Table {
	private char[][] tableOX = { { '_', '_', '_' }, { '_', '_', '_' }, { '_', '_', '_' } };
	private Player first;
	private Player second;
	private Player O;
	private Player X;
	private Player win;
	private Player player;
	private int countturn = 0;

	public Table(Player first, Player second) {
		this.first = first;
		this.second = second;
		this.player = first;
		this.O = first;
		this.X = second;
	}

	public Player getPlayer() {
		return player;
	}

	public void setTable(int row, int col) {
		tableOX[row - 1][col - 1] = player.getName();
		CheckWin();
		if (CheckWin() == false) {
			switchPlayer();
		}
	}

	public void switchPlayer() {
		if (player == first) {
			player = second;
		} else if (player == second) {
			player = first;
		}
	}

	public void printTable() {
		for (int i = 0; i < tableOX.length; i++) {
			for (int j = 0; j < tableOX.length; j++) {
				System.out.print(tableOX[i][j] + " ");
			}
			System.out.println();
		}
	}
	public boolean CheckWin() {
		if (CheckWinRow()) {
			win = player;
			return true;
		} else if (CheckWinCol()) {
			win = player;
			return true;
		} else if (CheckWinX()) {
			win = player;
			return true;
		}
		return false;
	}

	public boolean CheckWinRow() {
		if (tableOX[0][0] == player.getName() && tableOX[0][1] == player.getName()
				&& tableOX[0][2] == player.getName()) {
			return true;
		} else if (tableOX[1][0] == player.getName() && tableOX[1][1] == player.getName()
				&& tableOX[1][2] == player.getName()) {
			return true;
		} else if (tableOX[2][0] == player.getName() && tableOX[2][1] == player.getName()
				&& tableOX[2][2] == player.getName()) {
			return true;
		}

		return false;
	}

	public boolean CheckWinCol() {
		if (tableOX[0][0] == player.getName() && tableOX[1][0] == player.getName()
				&& tableOX[2][0] == player.getName()) {
			return true;
		} else if (tableOX[0][1] == player.getName() && tableOX[1][1] == player.getName()
				&& tableOX[2][1] == player.getName()) {
			return true;
		} else if (tableOX[0][2] == player.getName() && tableOX[1][2] == player.getName()
				&& tableOX[2][2] == player.getName()) {
			return true;
		}

		return false;
	}

	public boolean CheckDraw() {
		if (countturn == 9 && CheckWin() == false) {
			return true;
		}
		return false;
	}

	public boolean CheckWinX2() {
		for (int i = 0; i < tableOX.length; i++) {
			if (tableOX[i][2 - i] != player.getName()) {
				return false;
			}
		}
		return true;
	}
	public boolean CheckWinX() {
		if (CheckWinX1()) {
			return true;
		} else if (CheckWinX2()) {
			return true;
		}
		return false;
	}
	public boolean CheckWinX1() {
		for (int i = 0; i < tableOX.length; i++) {
			if (tableOX[i][i] != player.getName()) {
				return false;
			}
		}
		return true;
	}



	public boolean checkError(int row, int col) {
		if (row > 3 || row < 1 || col > 3 || col < 1) {
			System.out.println("Error Row : Please input 1, 2, 3");
			return true;
		} else if (tableOX[row - 1][col - 1] == 'O' || tableOX[row - 1][col - 1] == 'X') {
			System.out.println("You can't select this location " + "because it has already been chosen.");
			return true;
		}
		return false;
	}

	public void scoreUpdate() {
		if (win.getName() == 'O') {
			O.win();
			X.lose();
		} else if (win.getName() == 'X') {
			X.win();
			O.lose();
		} else if (CheckDraw()) {
			X.draw();
			O.draw();
		}
	}

	public Player getWinner() {
		return win;
	}

}
