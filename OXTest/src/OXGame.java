import java.util.InputMismatchException;
import java.util.Scanner;

public class OXGame {
	private Table table;
	private Player O;
	private Player X;
	private char again = 'Y';
	Scanner kb = new Scanner(System.in);

	public void startGame() {
		O = new Player('O');
		X = new Player('X');
		table = new Table(O, X);
	}

	public void showWelcome() {
		System.out.println("Welcome to OX game");
	}

	public void inputRowCol() {
		System.out.print("Please input row,col : ");
		try {
			int row = kb.nextInt();
			int col = kb.nextInt();
			if (table.checkError(row, col) == true) {
				inputRowCol();
			} else {
				table.setTable(row, col);
			}
		} catch (InputMismatchException e) {
			System.out.println("Input mismatch , try again");
		}

	}

	public void showTurn() {
		System.out.println("turn " + table.getPlayer().getName());
	}

	public void showTable() {
		this.table.printTable();
	}

	public void showScore() {
		System.out.println("X Win:" + X.getWin() + " Lost:" + X.getLose() + " Draw:" + X.getDraw());
		System.out.println("O Win:" + O.getWin() + " Lost:" + O.getLose() + " Draw:" + O.getDraw());
	}

	public void showWinner() {
		System.out.println(table.getWinner().getName() + " Win");
	}

	public void askPlayAgain() {
		System.out.println("You play agian (Y/N) : ");
		again = kb.next().charAt(0);
	}

	public void showBye() {
		System.out.println("Good bye...");
	}

	public void Play() {
		showWelcome();
		while (true) {
			this.showTable();
			showTurn();
			inputRowCol();

			if (table.CheckWin() || table.CheckDraw()) {
				showTable();
				this.table.scoreUpdate();
				showWinner();
				showScore();
				break;
			}
		}
		askPlayAgain();
		if (again == 'N') {
			showBye();
		}

	}
}
